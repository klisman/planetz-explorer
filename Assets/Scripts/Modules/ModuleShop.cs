﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class ModuleShop : MonoBehaviour {
	//Modules
	public GameObject modulePanel;
	public List<GameObject> mPanels = new List<GameObject>();
	public List<Module> modules = new List<Module>();
	//ModulesUpgrades
	public GameObject mUpgradeButton;
	public List<GameObject> mUpButtons = new List<GameObject> ();
	public List<ModuleUpgrade> mupgrades = new List<ModuleUpgrade> ();
	//
	ModuleDatabase mDatabase;
    GameObject gameManager;
    PlayerController playerc;

    // Use this for initialization
    void Start () {
		int pNumber = 0;
		mDatabase = GameObject.FindGameObjectWithTag("ModuleDatabase").GetComponent<ModuleDatabase>();
        gameManager = GameObject.Find("GameManager");
        playerc = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        foreach (Module mod in mDatabase.modules) {
			GameObject mp = (GameObject)Instantiate (modulePanel);
			mp.transform.SetParent (this.transform.GetChild(1).GetChild(0));
			mp.GetComponent<ModulePanel>().moduleID = pNumber;
			//
			mPanels.Add(mp); //Add panel to the list of panels
			modules.Add(mod); //Add module to the list of modules

			int mButtonCount = 0;
			foreach(ModuleUpgrade mup in mod.modUpgrades){
				GameObject mb = (GameObject)Instantiate (mUpgradeButton);
				mb.transform.SetParent (mPanels[pNumber].transform.Find("UpgradePanel"));
				mb.GetComponent<Image>().sprite = modules [pNumber].modUpgrades [mButtonCount].upgradeIcon;
				mb.GetComponent<ModuleUpgradeButton> ().ModuleID = pNumber;
				mb.GetComponent<ModuleUpgradeButton> ().UpgradeID = mButtonCount;
				mButtonCount++;
			}
			pNumber++;
		}
        //Ui
        gameManager.GetComponent<GameData>().moduleShop = gameObject.GetComponent<ModuleShop>();    
        gameObject.SetActive(false);
	
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void setMaxUpgrades() {
        foreach (Module module in modules) {
            if (module.modUpgrades.Any(u => u.bought == true))
            {
                int bestBoughtUpgrade = module.modUpgrades.Max(u => u.upgradeID);
                playerc.transform.GetChild(1).transform.GetChild(module.moduleID).GetComponent<SpriteRenderer>().color = modules[module.moduleID].modUpgrades[bestBoughtUpgrade].upgradeColor;
            }
            else {
                //Debug.Log("No Upgrade bought!");
            }
         //Debug.Log("UpgradesSet!");
        }
    }
}
