﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ModuleUpgradeButton : MonoBehaviour, IPointerEnterHandler{
	Module module;
	ModuleShop modShop;
	public int ModuleID;
	public int UpgradeID;
	Button btn;
	PlayerController playerc;

	// Use this for initialization
	void Start (){
		playerc = GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerController> ();
		modShop = GameObject.FindGameObjectWithTag ("ModuleShop").GetComponent<ModuleShop>();
		btn = this.gameObject.GetComponent<Button> ();
		btn.onClick.AddListener (() => BuyUpgrade ());
		//Debug.Log (modShop.modules[ModuleID].modUpgrades[UpgradeID].upgradeName);
	}
	
	// Update is called once per frame
	void Update (){
		if(modShop.modules[ModuleID].modUpgrades[UpgradeID].bought == true || modShop.modules[ModuleID].modUpgrades[UpgradeID].upgradeValue > GameData.balance)
        {
            gameObject.GetComponent<Button>().interactable = false;
        }
        else {
            gameObject.GetComponent<Button>().interactable = true;
        }
    }
	public void BuyUpgrade(){
		Debug.Log (ModuleID + " " + UpgradeID);
		if (GameData.balance >= modShop.modules[ModuleID].modUpgrades[UpgradeID].upgradeValue){
			Debug.Log ("Money ok");
			if(modShop.modules[ModuleID].modUpgrades[UpgradeID].bought == false){
				Debug.Log ("Não comprado ainda");
				if (modShop.modules[ModuleID].moduleLevel >= modShop.modules[ModuleID].modUpgrades[UpgradeID].upgradeReqLevel) {
					GameData.balance -= modShop.modules[ModuleID].modUpgrades[UpgradeID].upgradeValue;
					modShop.modules[ModuleID].modUpgrades[UpgradeID].bought = true;
					modShop.modules [ModuleID].moduleBaseHealth = modShop.modules [ModuleID].moduleBaseHealth * 1.05f;
					modShop.modules [ModuleID].moduleBaseDefense = modShop.modules [ModuleID].moduleBaseDefense * 1.05f;
					gameObject.GetComponent<Button> ().interactable = false;
					playerc.transform.GetChild (1).transform.GetChild (ModuleID).GetComponent<SpriteRenderer> ().color = modShop.modules [ModuleID].modUpgrades [UpgradeID].upgradeColor;
					Debug.Log ("Bought " + modShop.modules[ModuleID].moduleName + " Upgrade " + modShop.modules[ModuleID].modUpgrades[UpgradeID].upgradeName);
				}
			}

		}
	}
	public void OnPointerEnter(PointerEventData data){
		Debug.Log (modShop.modules[ModuleID].moduleName + "  " + modShop.modules[ModuleID].modUpgrades[UpgradeID].upgradeName +
			"  " + modShop.modules[ModuleID].modUpgrades[UpgradeID].bought + "  " + modShop.modules[ModuleID].modUpgrades[UpgradeID].upgradeValue);
	}

}

