﻿using UnityEngine;
using System.Collections;

public class ModuleUpgrade{
	public int upgradeID;
	public string upgradeName;
	public float upgradeHealth;
	public float upgradeDefense;
	public int upgradeReqLevel;
	public float upgradeValue;
	public Sprite upgradeIcon;
	public bool bought;
	public Color32 upgradeColor;

	public ModuleUpgrade(int id, string name, float health, float defense, int reqlevel, Color32 color, float value){
		upgradeID = id;
		upgradeName = name;
		upgradeHealth = health;
		upgradeDefense = defense;
		upgradeReqLevel = reqlevel;
		upgradeValue = value;
		upgradeIcon = Resources.Load<Sprite> ("UpgradesIcons/Upgrades_" + name);
		bought = false;
		upgradeColor = color;
	}
}

