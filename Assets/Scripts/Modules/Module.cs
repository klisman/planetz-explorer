﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Module{
	public int moduleID;
	public string moduleName;
	public int moduleLevel;
	public float moduleBaseHealth;
	public float moduleHealth;
	public float moduleBaseDefense;
	public float moduleDefense;
	public float moduleBaseCost;
	public float moduleCost;
	public Sprite moduleIcon;
	public List<ModuleUpgrade> modUpgrades;

	public Module(int id, string name, float baseHealth, float baseDefense, float baseCost, float cost){
		moduleID = id;
		moduleName = name;
		moduleLevel = 0;
		moduleBaseHealth = baseHealth;
		moduleHealth = 0;
		moduleBaseDefense = baseDefense;
		moduleDefense = 0;
		moduleBaseCost = baseCost;
		moduleCost = cost;
		moduleIcon = Resources.Load<Sprite> ("ModuleIcons/ModuleIcons_" + name);
		modUpgrades = new List<ModuleUpgrade> ();

		//Upgrades
		modUpgrades.Add (new ModuleUpgrade(0, "Iron_Upgrade", 5, 5, 25, new Color32(149, 165, 166, 255) ,25 * Mathf.Pow(baseCost, 1)));
		modUpgrades.Add (new ModuleUpgrade(1, "Steel_Upgrade", 5, 5, 50, new Color32(127, 140, 141, 255), 25 * Mathf.Pow(baseCost, 2)));
	}

}
