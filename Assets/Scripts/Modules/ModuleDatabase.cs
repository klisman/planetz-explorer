﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ModuleDatabase : MonoBehaviour{

	public List<Module> modules = new List<Module>();
	public List<ModuleUpgrade> mUpgrades = new List<ModuleUpgrade>();

	// Use this for initialization
	void Awake (){
		
		//Modules
		if(GameData.shipType == 1){
			modules.Add (new Module(0, "Shield", 7, 0, 5, 5));
			modules.Add (new Module(1, "Command Center", 0, 2, 100, 100));
			modules.Add (new Module(2, "Shell", 15, 0, 300, 300));
			modules.Add (new Module(3, "Warp Drive", 0, 30, 1000, 1000));
		}

		//Upgrades
	//	mUpgrades.Add (new ModuleUpgrade(0, "Iron Upgrade", 5, 5, 25, 100));
	//	mUpgrades.Add (new ModuleUpgrade(1, "Steel Upgrade", 5, 5, 50, 500));

		//Fills Modules with all upgrades
	//	for (int i = 0; i < modules.Count; i++) {
	//		for (int k = 0; k < mUpgrades.Count; k++) {
	//			modules[i].modUpgrades.Add (mUpgrades[k]);
	//		}
	//	}

		//Debug.Log (modules [0].moduleLevel);
		//Debug.Log (mUpgrades [0].upgradeName);
		//Debug.Log (modules [0].modUpgrades [0].upgradeName);

	}
	void Start(){
		
	}
	
	// Update is called once per frame
	void Update (){
	
	}
}

