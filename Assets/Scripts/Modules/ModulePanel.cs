﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ModulePanel : MonoBehaviour {
	//Dependencies
	public Module module; //Only reference, dont exist in this context
	ModuleShop modShop;
	public int moduleID;
	PlayerController playercontrol;
    //Displays
    public Image iconDisplay;
    public Text nameDisplay;
	public Text levelDisplay;
	public Text healthDisplay;
	public Text defenseDisplay;
	//Buy
	public Button buyModule;
	public Text buyModuleText;

	// Use this for initialization
	void Start () {
		modShop = GameObject.FindGameObjectWithTag ("ModuleShop").GetComponent<ModuleShop>();
		playercontrol = GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerController> ();
        iconDisplay = gameObject.transform.GetChild(0).GetComponent<Image>();
        nameDisplay = gameObject.transform.GetChild(1).GetComponent<Text>();
		levelDisplay = gameObject.transform.GetChild(2).GetComponent<Text>();
		healthDisplay = gameObject.transform.GetChild (3).GetComponent<Text> ();
		defenseDisplay = gameObject.transform.GetChild (4).GetComponent<Text> ();

		//Buy Button
		buyModule = gameObject.transform.GetChild(6).GetComponent<Button>();
		buyModuleText = buyModule.transform.GetChild (0).GetComponent<Text> ();
		buyModule.onClick.AddListener(() => BuyModule());

	}

	// Update is called once per frame
	void Update () {
        iconDisplay.sprite = modShop.modules[moduleID].moduleIcon;
        nameDisplay.text = modShop.modules[moduleID].moduleName;
		levelDisplay.text = modShop.modules[moduleID].moduleLevel.ToString();
		healthDisplay.text = modShop.modules [moduleID].moduleHealth.ToString("F1");
		defenseDisplay.text = modShop.modules [moduleID].moduleDefense.ToString ("F1");
		buyModuleText.text = "Buy\n " + modShop.modules[moduleID].moduleCost.ToString("F1");

	}

	public void BuyModule(){
		if (GameData.balance >= modShop.modules[moduleID].moduleCost) {
			GameData.balance -= modShop.modules [moduleID].moduleCost;
			modShop.modules [moduleID].moduleLevel++;
			modShop.modules [moduleID].moduleCost = modShop.modules [moduleID].moduleBaseCost * Mathf.Pow (1.15f, modShop.modules[moduleID].moduleLevel);
			//Health
			modShop.modules [moduleID].moduleHealth = modShop.modules [moduleID].moduleBaseHealth * Mathf.Pow (1.15f, modShop.modules [moduleID].moduleLevel);
			playercontrol.CalculateHealth ();
			//Defense
			modShop.modules [moduleID].moduleDefense = modShop.modules [moduleID].moduleBaseDefense * Mathf.Pow (1.15f, modShop.modules [moduleID].moduleLevel);
			playercontrol.CalculateDefense ();
		}
		//Debug.Log (playercontrol);
	}
}
