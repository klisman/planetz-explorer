﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Inventory : MonoBehaviour {

	public GameObject slotPrefab;
	public List<GameObject> Slots = new List<GameObject>();
	public List<Item> Items = new List<Item>();
	ItemDatabase database;
	GameObject gameManager;
	//Drag n Drop Items
	public GameObject toolTip; //Add in inspector
	public GameObject draggedItemGameObject;
	public bool draggingItem = false;
	public Item draggedItem;
	public int indexOfDragItem;


	// Use this for initialization
	void Start () {
		int slotAmount = 0;
		database = GameObject.FindGameObjectWithTag("ItemDatabase").GetComponent<ItemDatabase>();
        gameManager = GameObject.Find("GameManager");
        for (int i = 0; i < 25; i++) {
			GameObject slot = (GameObject)Instantiate (slotPrefab);
			slot.GetComponent<SlotScript> ().slotNumber = slotAmount;
			Slots.Add (slot);
			Items.Add (new Item ());
			slot.transform.SetParent (this.transform);
			slot.name = "Slot " + i;
			slotAmount++;
		}

        //Ui
        gameManager.GetComponent<GameData>().inventory = gameObject.GetComponent<Inventory>();
        gameObject.SetActive(false);
		//Debug.Log (Items[2].itemName);
		//Debug.Log(database.items[0].itemName);

	}
	
	// Update is called once per frame
	void Update () {
		//DragItem update
		if (draggingItem) {
			Vector3 pos = (Input.mousePosition - GameObject.FindGameObjectWithTag("Canvas").GetComponent<RectTransform>().localPosition);
			draggedItemGameObject.GetComponent<RectTransform> ().localPosition = new Vector3 (pos.x + 25, pos.y - 25, pos.z);
		}
	
	}
    //One item
	public void AddItem (int id){
		for(int i = 0; i < database.items.Count; i++){
			if (database.items[i].itemID == id) { //Check if id is valid
				Item item = database.items[i];
				item.itemAmount = 1;

				//Check if inventory already have the item
				CheckIfItemExist(id, item);
				break;
			}
		}
	}
    //Multiple items
    public void AddItem(int id, int amount)
    {
        for (int i = 0; i < database.items.Count; i++)
        {
            if (database.items[i].itemID == id)
            { //Check if id is valid
                Item item = database.items[i];
                item.itemAmount = amount;

                //Check if inventory already have the item
                CheckIfItemExist(id, item);
                break;
            }
        }

    }
    public void ClearInventory() {
        foreach (GameObject slot in Slots)
        {
            for(int i = 0; i < Items.Count; i++) {
                Items[i] = new Item();
            }
            slot.GetComponent<SlotScript>().item = new Item();
        }
    }

	public void AddItemAtEmptySlot(Item item){ //Look for a empty slot
		for (int i = 0; i < Items.Count; i++) {
			if (Items[i].itemName == null) {
				Items[i] = item;
				break;
			}
		}
		
	}

	public void CheckIfItemExist(int itemId, Item item){
		for (int i = 0; i < Items.Count; i++) {
			if (Items [i].itemID == itemId) {
				Items [i].itemAmount = Items[i].itemAmount + item.itemAmount;
				break;
			}
			else if(i == Items.Count - 1){
				AddItemAtEmptySlot(item);
			}
		}
	}

	public void ShowToolTip(Vector3 toolPos, Item item){
		toolTip.SetActive (true);
		toolTip.GetComponent<RectTransform> ().localPosition = new Vector3 (toolPos.x + 317, toolPos.y, toolPos.z);
		toolTip.transform.GetChild (0).GetComponent<Text>().text = item.itemName + " (" + item.itemAmount + ")"; 
		toolTip.transform.GetChild (1).GetComponent<Text>().text = item.itemDesc; 
		toolTip.transform.GetChild (2).GetComponent<Text>().text = "Value: " + item.itemValue.ToString(); 
	}

	public void closeToolTip(){
		toolTip.SetActive (false);
	}

	public void ShowDraggedItem(Item item, int slotNumber){
		indexOfDragItem = slotNumber;
		closeToolTip ();
		draggedItemGameObject.SetActive (true);
		draggedItem = item;
		draggingItem = true;
		draggedItemGameObject.GetComponent<Image> ().sprite = item.itemIcon;
		
	}
	public void CloseDraggedItem(){
		draggingItem = false;
		draggedItemGameObject.SetActive (false);
	}


}

