﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ItemDatabase : MonoBehaviour {

	public List<Item> items = new List<Item>();

	// Use this for initialization
	void Awake () {
		items.Add (new Item("Test_Item", 0, "Item for test only, if you see it please contact developer.", 0, 1, Item.ItemRarity.Seasonal));
		items.Add (new Item("Iron_Ingot", 1, "Iron metal ingot, commonly used in ship parts.", 1, 1, Item.ItemRarity.Common));
		items.Add (new Item("Steel_Ingot", 2, "Steel metal ingot, commonly used in ship parts.", 3, 1, Item.ItemRarity.Common));
		items.Add (new Item("Gold_Ingot", 3, "A valuable piece of metal.", 6, 1, Item.ItemRarity.Uncommon));
		items.Add (new Item("Basic_Solar_Panel", 4, "Used in ship to generate power.", 11, 1, Item.ItemRarity.Uncommon));
		items.Add (new Item("Glass", 5, "A clear piece of material.", 18, 1, Item.ItemRarity.Common));
		items.Add (new Item("Metal_Plate", 6, "Probably a wreck of a ship.", 26, 1, Item.ItemRarity.Uncommon));
		items.Add (new Item("Chipset_MK1", 7, "A valuable computer component.", 60, 1, Item.ItemRarity.Uncommon));
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

