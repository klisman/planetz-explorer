﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SlotScript : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler, IDragHandler{

	public Item item;
	Image itemImage;
	public int slotNumber;
	Inventory inventory;
	Text itemAmount;
	Transform sellButton;

	// Use this for initialization
	void Start () {
		inventory = GameObject.FindGameObjectWithTag ("Inventory").GetComponent<Inventory>();
		itemImage = gameObject.transform.GetChild (0).GetComponent<Image>();
		itemAmount = gameObject.transform.GetChild (1).GetComponent<Text>();
		sellButton = gameObject.transform.GetChild (2).GetComponent<Transform>();
	
	}
	
	// Update is called once per frame
	void Update () {
		
		if (inventory.Items[slotNumber].itemName != null) {
			itemAmount.enabled = false;
			item = inventory.Items [slotNumber];
			itemImage.enabled = true;
			itemImage.sprite = inventory.Items[slotNumber].itemIcon;
			itemAmount.enabled = true;
			itemAmount.text = inventory.Items[slotNumber].itemAmount.ToString();
			sellButton.gameObject.SetActive(true);
		} 
		else {
			itemImage.enabled = false;	
		}
	}

	public void OnPointerDown(PointerEventData data){
		if (inventory.Items [slotNumber].itemName == null && inventory.draggingItem) {
			inventory.Items [slotNumber] = inventory.draggedItem;
			inventory.CloseDraggedItem ();
		}
		else if (inventory.Items[slotNumber].itemName != null && inventory.draggingItem){
			inventory.Items[inventory.indexOfDragItem] = inventory.Items[slotNumber];
			inventory.Items [slotNumber] = inventory.draggedItem;
			inventory.CloseDraggedItem ();
		}
	}

	public void OnPointerEnter(PointerEventData data){
		if (inventory.Items [slotNumber].itemName != null && !inventory.draggingItem) {
			inventory.ShowToolTip (inventory.Slots[slotNumber].GetComponent<RectTransform>().localPosition, inventory.Items[slotNumber]);
		}
	}

	public void OnPointerExit(PointerEventData data){
		if (inventory.Items [slotNumber].itemName != null) {
			inventory.closeToolTip ();
		}
	}

	public void OnDrag(PointerEventData data){
		if (inventory.Items [slotNumber].itemName != null) {
			inventory.ShowDraggedItem (inventory.Items[slotNumber], slotNumber);
			inventory.Items [slotNumber] = new Item ();
			itemAmount.enabled = false;
			sellButton.gameObject.SetActive(false);
			inventory.closeToolTip ();
		}
	}
	public void SellItem(){
		//Check if have item
		if(inventory.Items [slotNumber].itemAmount > 0){
			inventory.Items [slotNumber].itemAmount--;
			GameData.balance += inventory.Items [slotNumber].itemValue;
			Debug.Log("Sold " + inventory.Items[slotNumber].itemName);
			//Check if no item left
			if (inventory.Items [slotNumber].itemAmount == 0) {
				inventory.Items [slotNumber] = new Item ();
				itemAmount.enabled = false;
				inventory.closeToolTip ();
				sellButton.gameObject.SetActive(false);
			}
		}
	}
}

