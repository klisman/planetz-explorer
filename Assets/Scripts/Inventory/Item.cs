﻿using UnityEngine;
using System.Collections;

public class Item{
	public int itemID;
	public string itemName;
	public string itemDesc;
	public float itemValue;
	public int itemAmount;
	public Sprite itemIcon;
	public ItemRarity itemRarity;

	public enum ItemRarity{
		Common,
		Uncommon,
		Rare,
		Mythical,
		Legendary,
		Ancient,
		Immortal,
		Seasonal
	}

	public Item(string name, int id, string desc, float value, int amount, ItemRarity rarity){
		itemName = name;
		itemID = id;
		itemDesc = desc;
		itemValue = value;
		itemAmount = amount;
		itemRarity = rarity;
		itemIcon = Resources.Load<Sprite> ("ItemIcons/ItemIcons_" + name);
	}

	//Empty Item for empty slots
	public Item(){
		
	}

}
