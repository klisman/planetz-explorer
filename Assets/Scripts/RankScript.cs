﻿using UnityEngine;
using System.Collections;

public class RankScript : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (GameData.currentFame >= GameData.maxFame)
        {
            LevelUp();
        }
        if (Input.GetKey(KeyCode.Z) && Debug.isDebugBuild)
        {
            GainFame(5);
        }
    }

    public void GainFame(float fameGain)
    {
        GameData.currentFame += fameGain;
        Debug.Log("Earned " + fameGain + " fame");
    }

    void LevelUp()
    {
        GameData.currentFame = 0;
        GameData.maxFame = GameData.maxFame * 1.2f;
        GameData.rank++;
        Debug.Log("Rank up!");
    }

}
