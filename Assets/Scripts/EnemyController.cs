﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EnemyController : MonoBehaviour {
	//STATS
	public float health;
	public float damage;
	public float fame;
	public int rankLevel;
	public PlayerController playercontrol;
	//MOVEMENT
	float rotationSpeed = 180f;
	float maxSpeed = 5f;
	Transform playerTrans;
	//SHOOT
	public float fireDelay = 1f;
	float cooldownTimer = 0;
	public GameObject enemyBulletPrefab;
	GameObject bullet;
	//Drop prefab
	public GameObject dropPrefab;
	//Display
	public Image healthBar;
	public float maxHealth;
	public Text rankDisplay;

    Quaternion rotation;
    // Use this for initialization
    void Awake() {
        //For canvas rotation issues
        rotation = gameObject.transform.GetChild(0).rotation;
    }
	void Start () {
		rankLevel = GameData.rank + Random.Range(0, 3);
		health = 6 * Mathf.Pow (1.2f, rankLevel + 1);
		maxHealth = health;
		damage = 4 * Mathf.Pow (1.2f, rankLevel + 1);
		fame = 7 * Mathf.Pow (1.2f, rankLevel + 1);
		playercontrol = GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerController> ();
	}

	void Update(){
		GetPlayerObj ();
		Chase ();
		Shoot ();
		Display ();
		Die ();
        FixCanvasRotation ();
	}

	//Fills player position
	void GetPlayerObj(){
		if (playerTrans == null) {
			GameObject go = GameObject.Find ("Player");
			if(go != null){
				playerTrans = go.transform;
			}
		}
		if (playerTrans == null) {
			return;
		}
	}

	//Pursuit of Happiness
	void Chase(){
		if(playerTrans != null){
		float distance = Vector2.Distance (playerTrans.position, transform.position);
		if (distance < 10) {
			//ROTATE
			Vector3 dir = playerTrans.position - transform.position;
			dir.Normalize ();
			float zAngle = Mathf.Atan2 (dir.y, dir.x) * Mathf.Rad2Deg - 90;
			Quaternion desiredRot = Quaternion.Euler (0, 0, zAngle);
			transform.rotation = Quaternion.RotateTowards (transform.rotation, desiredRot, rotationSpeed * Time.deltaTime);
			//MOVE
			Vector3 pos = transform.position;
			Vector3 velocity = new Vector3 (0, maxSpeed * Time.deltaTime, 0);
			pos += transform.rotation * velocity;
			transform.position = pos;
			}
		}
	}

	//Pew Pew Pew
	void Shoot(){
		if(playerTrans != null){
			float distance = Vector2.Distance (playerTrans.position, transform.position);
			if (distance < 10) {
				cooldownTimer -= Time.deltaTime;
				if (cooldownTimer <= 0) {
					cooldownTimer = fireDelay;
					Vector3 offset = transform.rotation * new Vector3 (0, 0.5f, 0);
					bullet = (GameObject)Instantiate (enemyBulletPrefab, transform.position + offset, transform.rotation);
					bullet.GetComponent<ProjectileScript> ().damage = damage;
					bullet.layer = 15; //15 = Enemy Projectile
					//Debug.Log ("Enemy Pew");
				}
			}
		}
	}

	//RECEIVE DAMAGE
	void OnTriggerEnter2D(Collider2D col) {
		//Colisão com Projetil inimigo
		if (col.gameObject.layer == 14) { //14 player bullet
			health -= playercontrol.playerDamage;
			Debug.Log ("Inimigo recebeu " + playercontrol.playerDamage + " de dano.");
		}
		//Colisão com nave de um player
		if (col.gameObject.layer == 8){//layer 8 player
			health -= health * 0.20f;
		}

		if (col.gameObject.layer == 11) {//layer 11 asteroid
			health -= health * 0.25f;
		}
	}

	void Display(){
		healthBar.fillAmount = health / maxHealth;
		rankDisplay.text = rankLevel.ToString ();
	}

	//Kills
	void Die(){
		if (health <= 0) {
			Destroy (gameObject);
			GameObject dropObject = (GameObject)Instantiate (dropPrefab);
			dropObject.transform.position = transform.position;
			//Set loot content
			DropScript drop = dropObject.GetComponent<DropScript> ();
			drop.GenerateDrop(rankLevel);
			drop.fame += fame;
		}
	}
	void FixCanvasRotation(){
		gameObject.transform.GetChild (0).position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y + 1, gameObject.transform.position.z);
        gameObject.transform.GetChild(0).rotation = rotation;
    }
}
