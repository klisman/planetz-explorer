﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    //Player Stats
    public float playerDamage;
    public float playerDefense;
    public float playerCurrentHealth;
    public float playerMaxHealth;
    //RebuildShip
    public bool wreck = false;

    public WeaponShop wshop;
    public ModuleShop modShop;
    public Inventory inventory;
    public GalaxyManager galaxyManager;

    //SHOOT
    public float fireRate = 0.3F;
    private float nextFire = 0.0F;
    public GameObject bulletPrefab;
    ProjectileScript bulletScript;
    GameObject bullet;

    //MOVEMENT
    private Vector3 mousePosition;
    float maxSpeed = 6f;
    float rotateSpeed = 180f;

    //Shop
    public static bool isOnPlanet;

    // Use this for initialization
    void Start()
    {
        wshop = GameObject.FindGameObjectWithTag("WeaponShop").GetComponent<WeaponShop>();
        modShop = GameObject.FindGameObjectWithTag("ModuleShop").GetComponent<ModuleShop>();
        inventory = GameObject.FindGameObjectWithTag("Inventory").GetComponent<Inventory>();
        galaxyManager = GameObject.Find("GameManager").GetComponent<GalaxyManager>();
        CalculateHealth();
        playerCurrentHealth = playerMaxHealth;
    }

    // Update is called once per frame
    void Update()
    {
        //Check Health
        Die();
        //
        if (wreck == false)
        {
            //
            MoveControl();
            //
            Shoot();
        }
        //Regen
        RepairShip();
        //
        DamagePlayer();
        //RebuildShip if wreck
        RebuildShip();
    }

    //Update with fixed time interval
    void FixedUpdate()
    {

    }


    //Instantiate Bullet and give a damage value
    void Shoot()
    {

        if (Input.GetButtonUp("Fire1") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Vector3 offset = transform.rotation * new Vector3(0, 0.20f, 0);
            bullet = (GameObject)Instantiate(bulletPrefab, transform.position + offset, RotateWeaponScript.weaponsRotation);
            bullet.layer = 14; //14 = PlayerProjectile
            bulletScript = bullet.transform.GetComponent<ProjectileScript>();
            bulletScript.damage = playerDamage;

        }
    }

    //MOVE THE SHIP
    void MoveControl()
    {
        //ROTATE the ship
        Quaternion rot = transform.rotation;
        float z = rot.eulerAngles.z;
        z -= Input.GetAxis("Horizontal") * rotateSpeed * Time.deltaTime;
        rot = Quaternion.Euler(0, 0, z);
        transform.rotation = rot;
        //MOVE
        Vector3 pos = transform.position;
        Vector3 velocity = new Vector3(0, Mathf.Abs(Input.GetAxis("Vertical")) * maxSpeed * Time.deltaTime, 0);
        pos += rot * velocity;
        transform.position = pos;
    }

    //Repair
    void RepairShip()
    {
        if (playerCurrentHealth < playerMaxHealth)
        {
            playerCurrentHealth += 2.5f * Time.deltaTime;
        }
    }
    void RebuildShip()
    {
        if (wreck == true && playerCurrentHealth < playerMaxHealth)
        {
            playerCurrentHealth += 5f * Time.deltaTime;
            Debug.Log("repairing " + wreck);
        }
        else
        {
            wreck = false;
        }
    }

    //Contato
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Planet")
        {
            isOnPlanet = true;
            Debug.Log("Entrou no planeta");
        }
        //Colisão com inimigo
        if (col.gameObject.layer == 10)
        { //10 enemy layer
            playerCurrentHealth -= playerCurrentHealth * 0.1f;
            Debug.Log("Hit by enemy Ship");
        }
        //Colisão com projetil inimigo
        if (col.gameObject.layer == 15)
        { //15 = enemy bullet
            playerCurrentHealth -= (col.GetComponent<ProjectileScript>().damage - playerDefense);
            Debug.Log("Atingido em " + col.GetComponent<ProjectileScript>().damage + " de dano!");
        }
        //Colisão com asteroid
        if (col.gameObject.tag == "Asteroid")
        {
            playerMaxHealth -= playerMaxHealth * 0.25f;
            Debug.Log("Hit by a asteroid!");
        }
    }

    //Enable and disable shops
    void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.tag == "Planet")
        {
            isOnPlanet = false;
        }
    }

    void Die()
    {
        if (playerCurrentHealth <= 0)
        {
            playerCurrentHealth = 0;
            GameData.deathsCount++;
            inventory.ClearInventory();
            GameData.balance -= GameData.balance * 0.25f;
            //Destroy(gameObject);
            wreck = true; //start the rebuild
            Debug.Log("E morreu... REMOVER");

        }
    }

    public void CalculateDamage()
    {
        float countDamage = 0;
        for (int i = 0; i < wshop.weapons.Count; i++)
        {
            countDamage += wshop.weapons[i].weaponDamage;
        }
        playerDamage = countDamage;
    }

    public void CalculateHealth()
    {
        float bonusHealth = 0;
        for (int i = 0; i < modShop.modules.Count; i++)
        {
            bonusHealth += modShop.modules[i].moduleHealth;
        }
        playerMaxHealth = 100 + bonusHealth;
    }

    public void CalculateDefense()
    {
        float countDefense = 0;
        for (int i = 0; i < modShop.modules.Count; i++)
        {
            countDefense += modShop.modules[i].moduleDefense;
        }
        playerDefense = countDefense;
    }
    //Test Fuctionm only development build
    void DamagePlayer()
    {
        if (Input.GetKey(KeyCode.K) && Debug.isDebugBuild)
        {
            playerCurrentHealth -= 20;
        }
    }
}
