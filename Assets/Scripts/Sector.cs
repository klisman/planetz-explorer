﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Sector {
	//Sector Properties
	public string sectorCode;
	public static int sectorWidth = 50;
	public Vector3 centerLoc;
	public Vector3 verticeA;
	public Vector3 verticeB;
	public Vector3 verticeC;
	public Vector3 verticeD;

	void Start(){
		
	}

	public Sector(Vector3 loc) {
		this.centerLoc = loc;
		//Generate vertices
		this.verticeA = new Vector3 (loc.x - 25, loc.y + 25, 0);
		this.verticeB = new Vector3 (loc.x + 25, loc.y + 25, 0); 
		this.verticeC = new Vector3 (loc.x + 25, loc.y - 25, 0); 
		this.verticeD = new Vector3 (loc.x - 25, loc.y - 25, 0); 
		//Generate SectorCODE
		this.sectorCode = loc.x.ToString() + loc.y.ToString() + loc.z.ToString();

	}
}