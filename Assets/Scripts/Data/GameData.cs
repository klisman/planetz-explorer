﻿using UnityEngine;
using System;

/// <summary>
/// Game data.
/// </summary>
public class GameData : MonoBehaviour
{

    public static GameData gameData;

    //Player
    public static string playerName = "Captain";
    public static float balance = 6; //Default start 12 to buy the first weapon.
    public static int shipType = 1; //Define ship size/layout
    public string saveTime;

    //Rank
    public static int rank = 0;
    public static float currentFame = 0;
    public static float maxFame = 100;

    //Statistics
    public static int travelledDistance;
    public static int enemiesKilled;
    public static int asteroidsDestroyed;
    public static int deathsCount;

    //Dependencies
    public PlayerController playerc;
    public WeaponShop WeaponShop;
    public ModuleShop moduleShop;
    public Inventory inventory;

    public void Start()
    {
        playerc = GameObject.Find("Player").GetComponent<PlayerController>();
        ZPlayerPrefs.Initialize("s1534a5ffv4s2", "th3sp4c31s4w3s0m3");
        InvokeRepeating("SaveGame", 0f, 60f);
        if(ZPlayerPrefs.GetString("saveTime") != string.Empty) { 
            LoadGame ();
        }
    }

    public void SaveGame()
    {
        //Player
        ZPlayerPrefs.SetFloat("balance", balance);
        ZPlayerPrefs.SetInt("shipType", shipType);
        ZPlayerPrefs.SetString("saveTime", DateTime.Now.ToString());
        //Rank
        ZPlayerPrefs.SetInt("rank", rank);
        ZPlayerPrefs.SetFloat("currentFame", currentFame);
        ZPlayerPrefs.SetFloat("maxFame", maxFame);

        //Weapons
        foreach (Weapon w in WeaponShop.weapons)
        {
            ZPlayerPrefs.SetString("Weapon" + w.weaponID, w.weaponLevel + ";" + w.weaponCost + ";" + w.weaponDamage + ";" + w.weapUpgrades[0].bought + ";" + w.weapUpgrades[1].bought);
            //Debug.Log(ZPlayerPrefs.GetString("Weapon" + w.weaponID));
        }
        //Module
        foreach (Module m in moduleShop.modules)
        {
            ZPlayerPrefs.SetString("Module" + m.moduleID, m.moduleLevel + ";" + m.moduleCost + ";" + m.moduleHealth + ";" + m.moduleDefense
                + ";" + m.modUpgrades[0].bought + ";" + m.modUpgrades[1].bought);
            //Debug.Log(ZPlayerPrefs.GetString("Module" + m.moduleID));
        }
        //Inventory
        string slotContent = string.Empty;
        foreach (GameObject slot in inventory.Slots)
        {
            if (slot.GetComponent<SlotScript>().item != null)
            {
                slotContent += slot.GetComponent<SlotScript>().item.itemID + ";" + slot.GetComponent<SlotScript>().item.itemAmount + "#";
            }
        }

        ZPlayerPrefs.SetString("Inventory", slotContent);
        ZPlayerPrefs.Save();
        Debug.Log("Game Saved " + DateTime.Now + " !");
    }

    public void LoadGame()
    {
        //Player
        balance = ZPlayerPrefs.GetFloat("balance");
        shipType = ZPlayerPrefs.GetInt("shipType");
        saveTime = ZPlayerPrefs.GetString("saveTime");
        //Rank
        rank = ZPlayerPrefs.GetInt("rank");
        currentFame = ZPlayerPrefs.GetFloat("currentFame");
        maxFame = ZPlayerPrefs.GetFloat("maxFame");


        //Weapons
        for (int i = 0; i < 2; i++)
        {
            string rawWeapon = ZPlayerPrefs.GetString("Weapon" + i);
            //Debug.Log (ZPlayerPrefs.GetString ("Weapon" + i));
            string[] weapOpts = rawWeapon.Split(';');
            WeaponShop.weapons[i].weaponLevel = Int32.Parse(weapOpts[0]);
            WeaponShop.weapons[i].weaponCost = float.Parse(weapOpts[1]);
            WeaponShop.weapons[i].weaponDamage = float.Parse(weapOpts[2]);
            WeaponShop.weapons[i].weapUpgrades[0].bought = weapOpts[3].Equals("True") ? true : false;
            WeaponShop.weapons[i].weapUpgrades[1].bought = weapOpts[4].Equals("True") ? true : false;
        }
        playerc.CalculateDamage();
        //Modules
        for (int i = 0; i < 3; i++)
        {
            string rawModule = ZPlayerPrefs.GetString("Module" + i);
            string[] moduleOptions = rawModule.Split(';');
            moduleShop.modules[i].moduleLevel = Int32.Parse(moduleOptions[0]);
            moduleShop.modules[i].moduleCost = float.Parse(moduleOptions[1]);
            moduleShop.modules[i].moduleHealth = float.Parse(moduleOptions[2]);
            moduleShop.modules[i].moduleDefense = float.Parse(moduleOptions[3]);
            moduleShop.modules[i].modUpgrades[0].bought = moduleOptions[4].Equals("True") ? true : false;
            moduleShop.modules[i].modUpgrades[1].bought = moduleOptions[5].Equals("True") ? true : false;
            moduleShop.setMaxUpgrades();
        }
        playerc.CalculateDefense();
        playerc.CalculateHealth();
        //Inventory
        inventory.ClearInventory();
        string rawInventory = ZPlayerPrefs.GetString("Inventory");
        string[] items = rawInventory.Split('#');
        foreach(string i in items)
        {
            string[] itemOpts = i.Split(';');
            inventory.AddItem(Int32.Parse(itemOpts[0]), Int32.Parse(itemOpts[1]));
            //Debug.Log(itemOpts[0] + " " + itemOpts[1]);
        }
        Debug.Log("Game Loaded");
    }
    public void HardReset()
    {
        PlayerPrefs.DeleteAll();
    }
}
