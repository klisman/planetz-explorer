﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DropScript : MonoBehaviour {
	//Content
	public int itemId;
	public float fame;
	public ItemDatabase database;
	//Magnetism
	public Transform target;
	private float range = 4;
	public List<Item> posItems;

	void Start() {
		target = GameObject.FindWithTag("Player").transform;

	}

	void Update() {
		if (InRange()) {
			Attract();
		}
	}

	private bool InRange() {
		return Vector2.Distance (transform.position, target.position) <= range;
	}

	private void Attract( ){
		transform.position = Vector2.Lerp (transform.position, target.position, Time.deltaTime); //* strength
	}


	public void GenerateDrop(int rank){
		database = GameObject.FindGameObjectWithTag("ItemDatabase").GetComponent<ItemDatabase>();
		int rankL = rank;
		posItems = new List<Item>();
		if(rankL < 10){
			posItems = database.items.FindAll(i => i.itemRarity == Item.ItemRarity.Common);
			int x = Random.Range (1, posItems.Count); //Remove 0 Teste_Item
			itemId = posItems[x].itemID;
			Debug.Log ("Drop: " + posItems [x].itemName);
		}
	}
}
