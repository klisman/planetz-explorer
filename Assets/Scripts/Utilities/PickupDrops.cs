﻿using UnityEngine;
using System.Collections;

public class PickupDrops : MonoBehaviour {
	
	Inventory inventory;
	private DropScript dropscript;
	private RankScript rankScript;

	// Use this for initialization
	void Start () {
		inventory = GameObject.FindGameObjectWithTag ("Inventory").GetComponent<Inventory> ();
		rankScript = GameObject.Find ("GameManager").GetComponent<RankScript> ();
	}
	
	void OnTriggerEnter2D(Collider2D other){
		dropscript = other.gameObject.GetComponent<DropScript>();
		inventory.AddItem (dropscript.itemId, 1);
		rankScript.GainFame(dropscript.fame);
		Destroy (other.gameObject);
	}
}
