﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SettingsScript : MonoBehaviour {

	public static bool soundsEnabled;
	public static bool musicEnabled;
	public Button saveButton;
	public Button loadButton;
	public Button clearButton;
	GameData gamedata;

	// Use this for initialization
	void Start () {
		gamedata = GameObject.Find ("GameManager").GetComponent<GameData>();
		saveButton.onClick.AddListener(() => gamedata.SaveGame());
		loadButton.onClick.AddListener(() => gamedata.LoadGame());
		clearButton.onClick.AddListener(() => gamedata.HardReset());
		//UI
		gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

}
