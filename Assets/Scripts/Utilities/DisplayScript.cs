﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DisplayScript : MonoBehaviour {
	//Health
	public Image Bar;
	public Text healthText;
	//Rank
	public Image RankBar;
	public Text rankText;
	public Text rankLv;
    //Status
    public Image wreckImage;

	//Dependencie
	PlayerController playercontrol;

	// Use this for initialization
	void Start () {
		//Dependencies
		playercontrol = GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerController> ();
		Bar = gameObject.transform.GetChild(0).GetComponent<Image>();
		healthText = gameObject.transform.GetChild(2).GetComponent<Text>();
		RankBar = gameObject.transform.GetChild (3).GetComponent<Image>();
		rankText = gameObject.transform.GetChild (5).GetComponent<Text>();
		rankLv = gameObject.transform.GetChild (6).GetComponent<Text>();
        wreckImage = gameObject.transform.GetChild(8).GetComponent<Image>();
        //HealthFill
        playercontrol.playerCurrentHealth = playercontrol.playerMaxHealth;

	}
	
	// Update is called once per frame
	void Update () {
		//HealthFill
		Bar.fillAmount = playercontrol.playerCurrentHealth / playercontrol.playerMaxHealth;
		healthText.text = playercontrol.playerCurrentHealth.ToString("F0") + " / " + playercontrol.playerMaxHealth.ToString("F0");

		//RankFill
		RankBar.fillAmount = GameData.currentFame / GameData.maxFame;
		rankText.text = GameData.currentFame.ToString("F0") + " / " + GameData.maxFame.ToString("F0");
		rankLv.text = "Rank " + GameData.rank;

        //Status
        //Wreck
        if(playercontrol.wreck == true)
        {
            wreckImage.enabled = true;
        }
        else
        {
            wreckImage.enabled = false;
        }
	}
}
