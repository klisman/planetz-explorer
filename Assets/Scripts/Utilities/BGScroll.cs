﻿using UnityEngine;
using System.Collections;

public class BGScroll : MonoBehaviour {

	public float paralax = 2f;

	void Update () {
		MeshRenderer mr = GetComponent<MeshRenderer> ();
		Material mat = mr.material;
		Vector2 offset = mat.mainTextureOffset;

		offset.x = transform.position.x / transform.localScale.x / paralax;
		offset.y = transform.position.y / transform.localScale.y / paralax;

		mat.mainTextureOffset = offset;
	}
}
