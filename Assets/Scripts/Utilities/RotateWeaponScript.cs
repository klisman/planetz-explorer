﻿using UnityEngine;
using System.Collections;

public class RotateWeaponScript : MonoBehaviour {

	public static Quaternion weaponsRotation;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 mousePos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
		transform.rotation = Quaternion.LookRotation (Vector3.forward, mousePos - transform.position);
		weaponsRotation = transform.rotation;
	}
}
