﻿using UnityEngine;
using System.Collections;

public class ProjectileScript : MonoBehaviour {
	int projHealth = 1;
	float maxSpeed = 10;
	float timer = 3f;
	public float damage;

	// Use this for initialization
	void Start () {
		Destroy(gameObject, timer);
	}

	// Update is called once per frame
	void Update () {
		MoveProj ();	
		//Die contition
		if (projHealth <= 0) {
			Die ();
		}
		
	}
	//DAMAGE
	void OnTriggerEnter2D(Collider2D col) {
		if (col.gameObject.tag == "Player" || col.gameObject.tag == "Enemy" || col.gameObject.tag == "Asteroid") {
			projHealth -= 1;
			GameObject preFab = Resources.Load ("BulletExplosion") as GameObject;
			GameObject explode = Instantiate (preFab) as GameObject;
			explode.transform.position = transform.position;
			Destroy (explode, 0.4f);
		}
	}
	//MOVE
	void MoveProj(){
		Vector3 pos = transform.position;
		Vector3 velocity = new Vector3(0, maxSpeed * Time.deltaTime, 0);
		pos += transform.rotation * velocity;
		transform.position = pos;
	}

	void Die(){
		Destroy (gameObject);
	}
}
