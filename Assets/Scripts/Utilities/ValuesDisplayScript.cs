﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ValuesDisplayScript : MonoBehaviour {
	//Dependencies
	public Text balanceDisplay;
	public Text damageDisplay;
	public Text defenseDisplay;
	public PlayerController playercontrol;

	void Start(){
		playercontrol = GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerController> ();
		balanceDisplay = gameObject.transform.GetChild(0).GetComponent<Text>();
		damageDisplay = gameObject.transform.GetChild(2).GetComponent<Text>();
		defenseDisplay = gameObject.transform.GetChild (4).GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
		//Balance Display
		balanceDisplay.text = GameData.balance.ToString("F");
		damageDisplay.text = playercontrol.playerDamage.ToString("F");
		defenseDisplay.text = playercontrol.playerDefense.ToString ("F");
	}
}
