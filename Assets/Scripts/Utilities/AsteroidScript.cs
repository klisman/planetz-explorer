﻿using UnityEngine;
using System.Collections;

public class AsteroidScript : MonoBehaviour {
	float health = 3;

	void Update(){
		Die ();
	}
	void OnTriggerEnter2D(Collider2D col) {
		//health -= GameData.damage;
		GameObject preFab = Resources.Load ("BulletExplosion") as GameObject;
		GameObject explode = Instantiate (preFab) as GameObject;
		explode.transform.position = transform.position;
        Destroy(gameObject);
		Destroy (explode, 0.4f);
	}
	void Die(){
		if (health <= 0) {
			Destroy (gameObject);
			GameData.asteroidsDestroyed++;
		}
	}
}
