﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {

	public Transform playerObject;
	// Update is called once per frame
	void Start(){
		playerObject = GameObject.Find ("Player").transform;
	}
	void Update () {
		if(playerObject != null){
			Vector3 targPos = playerObject.position;
			targPos.z = transform.position.z;
			transform.position = targPos;
		}	
	}
}
