﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Galaxy manager responsible for the generation of the world.
/// </summary>
public class GalaxyManager : MonoBehaviour {

	//Player Object
	public GameObject player;
	public static List<Sector> sectors;
	public GameObject planetPrefab;
	public GameObject[] asteroids;
	public GameObject[] enemies;
	//Planet Color List
	public Color32[] planetColors;
	public Sprite[] planetModels;
	//Displays



	// Use this for initialization
	void Start () {
		//Spawn player
		player = SpawnPlayer ();
		//Planet Models
		planetModels = new Sprite[6];
		planetModels[0] = Resources.Load<Sprite>("PlanetModels/PlanetBase00");
		planetModels[1] = Resources.Load<Sprite>("PlanetModels/PlanetBase01");
		planetModels[2] = Resources.Load<Sprite>("PlanetModels/PlanetBase02");
		planetModels[3] = Resources.Load<Sprite>("PlanetModels/PlanetBase03");
		planetModels[4] = Resources.Load<Sprite>("PlanetModels/PlanetBase04");
		planetModels[5] = Resources.Load<Sprite>("PlanetModels/PlanetBase05");

		//Planet Colors
		planetColors = new Color32[4];
		planetColors [0] = new Color32(149, 165, 166, 255); //Concrete
		planetColors[1] = new Color32(230, 126, 34, 255); //Carrot
		planetColors[2] = new Color32(46, 204, 113, 255); //Emerald
		planetColors[3] = new Color32(52, 152, 219, 255); //Peter River

		//Start sectors list
		sectors = new List<Sector>();
		sectors.Add (new Sector (new Vector3 (0, 0, 0)));
	}
	
	// Update is called once per frame
	void Update () {	
		GenerateGalaxy ();
	}

	// Instantiate the player
	public GameObject SpawnPlayer(){
		GameObject playerObj = (GameObject)Instantiate (Resources.Load<GameObject>("Type" + GameData.shipType + "Ship"), new Vector3 (0, 0, 5), Quaternion.identity);
		playerObj.transform.parent = GameObject.Find ("Player").transform;
        //Debug.Log("PlayerSpawned!");
		return playerObj;
	}

	//Procedural generate Galaxy
	void GenerateGalaxy(){
		Sector playerSector = getPlayerSector();
		//Top sector
		Vector3 topSectorPos = playerSector.centerLoc + new Vector3 (0, 50, 0);
		if (!checkSector (topSectorPos)) {
			sectors.Add (new Sector(topSectorPos));
			fillSector (topSectorPos);
			//Debug.Log("Setor acima adicionado");
		}
		//Right sector
		Vector3 rightSectorPos = playerSector.centerLoc + new Vector3 (50, 0, 0);
		if (!checkSector (rightSectorPos)) {
			sectors.Add (new Sector (rightSectorPos));
			fillSector (rightSectorPos);
			//Debug.Log ("Setor direito adicionado");
		}
		//Botton sector
		Vector3 bottonSectorPos = playerSector.centerLoc + new Vector3 (0, -50, 0);
		if (!checkSector (bottonSectorPos)) {
			sectors.Add (new Sector (bottonSectorPos));
			fillSector (bottonSectorPos);
			//Debug.Log ("Setor abaixo adicionado");
		}
		//Left sector
		Vector3 leftSectorPos = playerSector.centerLoc + new Vector3 (-50, 0, 0);
		if (!checkSector (leftSectorPos)) {
			sectors.Add (new Sector (leftSectorPos));
			fillSector (leftSectorPos);
			//Debug.Log ("Setor esquerdo adicionado");
		}
	}

	//Fill the Sector with planets, stars, meteors, etc
	public void fillSector(Vector3 sectorPos){
		//Generate random position to Planet
		float randX = Random.Range (sectorPos.x + 25, sectorPos.x - 25);
		float randY = Random.Range (sectorPos.y + 25, sectorPos.y - 25);
		GameObject planetGO = (GameObject)Instantiate (planetPrefab, new Vector3 (randX, randY, 10), Quaternion.identity);
		planetGO.GetComponent<SpriteRenderer> ().sprite = planetModels [Random.Range (0, planetModels.Length)];
		planetGO.GetComponent<SpriteRenderer>().color = planetColors[Random.Range(0, planetColors.Length)];

		//Ganerate a random number of asteriods and instantiate
		int AstNumber = Random.Range(3, 7);
		for (int i = 0; i <= AstNumber; i++) {
			//POSITION
			float randAX = Random.Range (sectorPos.x + 25, sectorPos.x - 25);
			float randAY = Random.Range (sectorPos.y + 25, sectorPos.y - 25);
			//TYPE
			int randAsteroid = Random.Range (0, asteroids.Length);
			GameObject asteroid = (GameObject)Instantiate (asteroids[randAsteroid], new Vector3 (randAX, randAY, 10), Quaternion.identity);
			asteroid.AddComponent <AsteroidScript>();
		}

		//Generate a random number of enemies and instantiate
		int EnemiesNumber = Random.Range(1,3);
		for (int i = 0; i <= EnemiesNumber; i++) {
			//POSITION
			float randEX = Random.Range (sectorPos.x + 25, sectorPos.x - 25);
			float randEY = Random.Range (sectorPos.y + 25, sectorPos.y - 25);
			//TYPE
			int randEnemy = Random.Range(0, enemies.Length);
			Instantiate (enemies [randEnemy], new Vector3 (randEX, randEY, 5), Quaternion.identity);
		}
	}

	//Return player sector
	public Sector getPlayerSector(){
		Vector3 playerPos = player.transform.position;
		foreach (Sector sector in sectors) {
			Bounds sectorBound = new Bounds (sector.centerLoc, new Vector3(50, 50, 10));
			if (sectorBound.Contains (playerPos)) {
				return sector;
			}
		}
		return null;
	}

	//Return if the Sector exists
	public bool checkSector(Vector3 possiblePos){
		foreach (Sector sector in sectors) {
			if (sector.centerLoc == possiblePos) {
				return true;
			}
		}
		return false;
	}
}
