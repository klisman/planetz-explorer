﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class WeaponShop : MonoBehaviour {
	//Weapons
	public GameObject weaponPanel;
	public List<GameObject> wPanels = new List<GameObject>();
	public List<Weapon> weapons = new List<Weapon>();
	//WeaponsUpgrades
	public GameObject wUpgradeButton;
	public List<GameObject> wUpButtons = new List<GameObject>();
	public List<WeaponUpgrade> wUpgrades = new List<WeaponUpgrade>();

	//Dependencies
	WeaponDatabase wDatabase;
	GameObject gameManager;

	// Use this for initialization
	void Start () {
		int wNumber = 0;
		wDatabase = GameObject.FindGameObjectWithTag ("WeaponDatabase").GetComponent<WeaponDatabase>();
		gameManager = GameObject.Find ("GameManager");
		foreach (Weapon weap in wDatabase.weapons) {
			GameObject wpanel = (GameObject)Instantiate (weaponPanel);
			wpanel.transform.SetParent (this.transform.GetChild(1).GetChild(0));
			wpanel.GetComponent<WeaponPanel> ().weaponID = wNumber;
			//
			wPanels.Add (wpanel);
			weapons.Add (weap);
			int wButtonCount = 0;
			foreach (WeaponUpgrade weup in weap.weapUpgrades) {
				GameObject wB = (GameObject)Instantiate (wUpgradeButton);
				wB.transform.SetParent (wPanels [wNumber].transform.Find ("WeaponUpgradePanel"));
				wB.GetComponent<Image> ().sprite = weapons [wNumber].weapUpgrades [wButtonCount].upgradeIcon;
				wB.GetComponent<WeaponUpgradeButton> ().WeaponID = wNumber;
				wB.GetComponent<WeaponUpgradeButton> ().UpgradeID = wButtonCount;
				wButtonCount++;
			}
			wNumber++;
		}
		//Ui
		gameManager.GetComponent<GameData>().WeaponShop = gameObject.GetComponent<WeaponShop>();
		gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
