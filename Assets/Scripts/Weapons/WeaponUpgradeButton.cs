﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class WeaponUpgradeButton : MonoBehaviour{
	Weapon weapon;
	WeaponShop wshop;
	public int WeaponID;
	public int UpgradeID;
	Button btn;

	// Use this for initialization
	void Start () {
		wshop = GameObject.FindGameObjectWithTag ("WeaponShop").GetComponent<WeaponShop>();
		btn = this.gameObject.GetComponent<Button> ();
		btn.onClick.AddListener (() => BuyUpgrade ());
	}
	
	// Update is called once per frame
	void Update () {
		if (wshop.weapons [WeaponID].weapUpgrades[UpgradeID].bought == true || wshop.weapons[WeaponID].weapUpgrades[UpgradeID].upgradeValue > GameData.balance) {
			gameObject.GetComponent<Button> ().interactable = false;
		} else {
			gameObject.GetComponent<Button> ().interactable = true;
		}
	}
	void BuyUpgrade(){
		if(GameData.balance >= wshop.weapons[WeaponID].weapUpgrades[UpgradeID].upgradeValue){
			Debug.Log ("Money ok");
			if(wshop.weapons[WeaponID].weapUpgrades[UpgradeID].bought == false){
				Debug.Log ("Nao comprado ainda");
				if( wshop.weapons[WeaponID].weaponLevel >=  wshop.weapons[WeaponID].weapUpgrades[UpgradeID].upgradeReqLevel){
					Debug.Log ("Upgrade no level necessário");
					GameData.balance -= wshop.weapons [WeaponID].weapUpgrades [UpgradeID].upgradeValue;
					wshop.weapons [WeaponID].weapUpgrades [UpgradeID].bought = true;
					wshop.weapons [WeaponID].weaponBaseDamage = wshop.weapons [WeaponID].weaponBaseDamage * 1.05f;
					gameObject.GetComponent<Button> ().interactable = false;
					//TODO Change weapon appearance
					Debug.Log("Comprado");
				}
			}
		}
	}
}

