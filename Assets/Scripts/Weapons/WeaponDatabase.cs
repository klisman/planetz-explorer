﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WeaponDatabase : MonoBehaviour {

	public List<Weapon> weapons = new List<Weapon>();
	public List<WeaponUpgrade> wUpgrades = new List<WeaponUpgrade>();

	// Use this for initialization
	void Awake () {
		//Weapons
		weapons.Add(new Weapon(0, "Laser Gun", 1, 6, 6));
		weapons.Add(new Weapon(1, "Piercing Gun", 16, 200, 200));
        weapons.Add(new Weapon(2, "Double Laser Gun", 31, 2000, 2000));
        weapons.Add(new Weapon(3, "Impact Gun", 80, 10400, 10400));
        weapons.Add(new Weapon(4, "Strong Laser Gun", 114, 24000, 24000));
        weapons.Add(new Weapon(5, "Rocket Launcher", 302, 109300, 109300));
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
