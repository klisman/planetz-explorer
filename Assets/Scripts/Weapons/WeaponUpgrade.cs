﻿using UnityEngine;
using System.Collections;

public class WeaponUpgrade{
	public int upgradeID;
	public string upgradeName;
	public float upgradeDamage;
	public int upgradeReqLevel;
	public float upgradeValue;
	public Sprite upgradeIcon;
	public bool bought;

	public WeaponUpgrade(int id, string name, float damage,int reqlevel, float value){
		upgradeID = id;
		upgradeName = name;
		upgradeDamage = damage;
		upgradeReqLevel = reqlevel;
		upgradeValue = value;
		upgradeIcon = Resources.Load<Sprite> ("UpgradesIcons/Upgrades_" + name);
		bought = false;
	}
}

