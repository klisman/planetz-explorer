﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class WeaponPanel : MonoBehaviour
{
    //Dependencies
    public Weapon weapon; //Reference
    WeaponShop weapShop;
    public int weaponID;
    PlayerController playercontrol;
    //Displays
    public Text nameDisplay;
    public Text levelDisplay;
    public Button buyWeapon;
    public Text buyWeaponText;
    public Text damageDisplay;
    public Image iconDisplay;
    public Toggle toggle;

    // Use this for initialization
    void Start()
    {
        weapShop = GameObject.FindGameObjectWithTag("WeaponShop").GetComponent<WeaponShop>();
        playercontrol = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        iconDisplay = gameObject.transform.GetChild(0).GetComponent<Image>();
        nameDisplay = gameObject.transform.GetChild(1).GetComponent<Text>();
        levelDisplay = gameObject.transform.GetChild(2).GetComponent<Text>();
        damageDisplay = gameObject.transform.GetChild(3).GetComponent<Text>();
        toggle = gameObject.transform.GetChild(3).GetComponent<Toggle>();

        //BuyButton
        buyWeapon = gameObject.transform.GetChild(5).GetComponent<Button>();
        buyWeaponText = buyWeapon.transform.GetChild(0).GetComponent<Text>();
        buyWeapon.onClick.AddListener(() => BuyWeapon());

    }

    // Update is called once per frame
    void Update()
    {
        iconDisplay.sprite = weapShop.weapons[weaponID].weaponIcon;
        nameDisplay.text = weapShop.weapons[weaponID].weaponName;
        levelDisplay.text = weapShop.weapons[weaponID].weaponLevel.ToString();
        damageDisplay.text = weapShop.weapons[weaponID].weaponDamage.ToString("F");
        buyWeaponText.text = "Buy\n" + weapShop.weapons[weaponID].weaponCost.ToString("F1");

    }

    public void BuyWeapon()
    {
        if (GameData.balance >= weapShop.weapons[weaponID].weaponCost)
        {
            GameData.balance -= weapShop.weapons[weaponID].weaponCost;
            weapShop.weapons[weaponID].weaponLevel++;
            weapShop.weapons[weaponID].weaponCost = weapShop.weapons[weaponID].weaponBaseCost * Mathf.Pow(1.15f, weapShop.weapons[weaponID].weaponLevel);
            weapShop.weapons[weaponID].weaponDamage = weapShop.weapons[weaponID].weaponBaseDamage * Mathf.Pow(1.15f, weapShop.weapons[weaponID].weaponLevel);
            playercontrol.CalculateDamage();
        }
        Debug.Log(playercontrol.playerDamage);
    }

}
