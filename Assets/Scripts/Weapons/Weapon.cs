﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Weapon{
	public int weaponID;
	public string weaponName;
	public int weaponLevel;
	public float weaponDamage;
	public float weaponBaseDamage;
	public float weaponBaseCost;
	public float weaponCost;
	public Sprite weaponIcon;
	public List<WeaponUpgrade> weapUpgrades;

	public Weapon(int id, string name, float basedamage, float basecost, float cost){
		weaponID = id;
		weaponName = name;
		weaponLevel = 0;
		weaponBaseDamage = basedamage;
		weaponDamage = 0;
		weaponBaseCost = basecost;
		weaponCost = cost;
		weaponIcon = Resources.Load<Sprite> ("Weapons/Weapons_" + name);
		weapUpgrades = new List<WeaponUpgrade> ();
		weapUpgrades.Add (new WeaponUpgrade (0, "Iron_Upgrade", 5, 25, 100));
		weapUpgrades.Add (new WeaponUpgrade (0, "Steel_Upgrade", 5, 50, 1000));

    }
}